#!/usr/bin/env python3 
"""Update levels species folowing the folder names"""

import json
import glob
import os

SPECIES_FOLDER_NAMES = list(map(os.path.basename, glob.glob("./static/data/src_audio/*")))

def filter(species, condition):
    keeped = []
    for name in species:
        if condition(name):
            keeped.append(name)
    return keeped

def only_matched_folder(species_name):
    folder_like_name = species_name.replace(' ', '_').replace('\'', '')
    folder_like_name = folder_like_name.lower()
    return folder_like_name in SPECIES_FOLDER_NAMES

def main():
    with open("./data/level_species.json", "r") as f:
        data = json.load(f)
    for level in data:
        species_list = data[level]
        species_list = filter(species_list, only_matched_folder)
        data[level] = species_list
    with open("./data/level_species_cleaned.json", "w") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    main()