#!/usr/bin/env python3
"""Generate species list for each levels"""

import os
import glob
import json
import math

LEVELS = 10

def load_species_sightings(file):
    species_sightings = {}
    with open(file, "r") as f:
        data = json.load(f)
    sightings = data['data']['sightings']
    counter = 0
    for sighting in sightings:
        species = sighting["species"]["name"]
        if species in species_sightings:
            species_sightings[species] += 1
        else:
            species_sightings[species] = 1
        counter += 1
    for species in species_sightings:
        species_sightings[species] /= counter * 0.01
    return species_sightings

def split_species_list(species_sightings):
    level_lists = { level: [] for level in range(1, LEVELS + 1)}
    species_sorted = sorted(species_sightings, key = lambda species: -species_sightings[species])
    species_number = len(species_sorted)
    species_per_level = species_number // LEVELS
    species_splitted = [species_sorted[i:i+species_per_level] for i in range(0, species_number, species_per_level)]
    for level in range(1, LEVELS + 1):
        level_lists[level] = species_splitted[level - 1]
    return level_lists

def main():
    file = "./data/export_26052022_150619.json"
    species_frequency = load_species_sightings(file)
    level_species = split_species_list(species_frequency)
    with open("./data/level_species.json", "w") as f:
        json.dump(level_species, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    main()